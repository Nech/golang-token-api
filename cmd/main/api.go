package main

import (
	"gitlab.com/Nech/golang-token-api/internal/composite"
	"gitlab.com/Nech/golang-token-api/internal/config"
)

func main() {
	cfg, err := config.Setup()
	if err != nil {
		panic(err)
	}

	err = composite.Run(cfg)
	if err != nil {
		panic(err)
	}
}
