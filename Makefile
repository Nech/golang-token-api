.PHONY:
.SILENT:

build:
	go build -o ./.bin/api cmd/main/api.go
run: build
	./.bin/api
test:
	go test ./... -v
