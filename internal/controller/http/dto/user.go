package dto

// LoginUserDTO describes the data structure for user login
type LoginUserDTO struct {
	Nickname string `json:"nickname" validate:"required,min=3,max=16,alphanum"`
	Password string `json:"password" validate:"required,min=7,max=16"`
}
