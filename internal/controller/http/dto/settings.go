package dto

type UpdateSettingsDTO struct {
	Lang        string `json:"lang" validate:"required,oneof=ukr eng"`
	Ripple      bool   `json:"ripple" validate:"boolean"`
	InputStyle  string `json:"input_style" validate:"required,oneof=outlined filled"`
	MenuMode    string `json:"menu_mode" validate:"required,oneof=static overlay"`
	ColorScheme string `json:"color_scheme" validate:"required,oneof=light dark"`
	Theme       string `json:"theme" validate:"required"`
	Scale       int32  `json:"scale" validate:"required"`
}
