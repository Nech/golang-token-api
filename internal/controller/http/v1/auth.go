package v1

import (
	"net/http"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/Nech/golang-token-api/internal/controller/http/dto"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
)

// signIn allows the user to log in and get an authorization tokens
func (h *Handler) signIn(c echo.Context) error {
	d := new(dto.LoginUserDTO)
	if err := c.Bind(d); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "could not decode data")
	}
	if err := c.Validate(d); err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	tokens, err := h.useCase.Login(d)
	if err != nil {
		return h.handleError(err, "could not get tokens due error")
	}

	return c.JSON(http.StatusCreated, tokens)
}

// refreshToken validates the passed refresh token and generates a new access token
func (h *Handler) refreshToken(c echo.Context) error {
	d := new(dto.RefreshTokenDTO)
	if err := c.Bind(d); err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "could not decode refresh token")
	}
	if err := c.Validate(d); err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, err)
	}

	token, err := h.useCase.UpdateAccessToken(d.RefreshToken)
	if err != nil {
		return h.handleError(err, "could not update token due error")
	}

	return c.JSON(http.StatusCreated, token)
}

// signOut deletes cached tokens of the user
func (h *Handler) signOut(c echo.Context) error {
	token := c.Get("user").(*jwt.Token)
	claims := token.Claims.(*entity.TokenClaims)

	h.useCase.Logout(claims.ID)

	return c.NoContent(http.StatusOK)
}
