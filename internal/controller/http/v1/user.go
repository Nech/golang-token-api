package v1

import (
	"net/http"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/Nech/golang-token-api/internal/controller/http/dto"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
)

// getUsers returns a list of paginated user profiles
func (h *Handler) getUsers(c echo.Context) error {
	users, err := h.useCase.GetAllUsers()
	if err != nil {
		return h.handleError(err, "could not get users due internal error")
	}

	return c.JSON(http.StatusOK, users)
}

// getUserByID returns the user profile by the given ID
func (h *Handler) getUserByID(c echo.Context) error {
	id := c.Param("id")
	user, err := h.useCase.GetUserByID(id)
	if err != nil {
		return h.handleError(err, "could not get user due internal error")
	}

	return c.JSON(http.StatusOK, user)
}

func (h *Handler) getUserByToken(c echo.Context) error {
	token := c.Get("user").(*jwt.Token)
	claims := token.Claims.(*entity.TokenClaims)
	userID := claims.ID

	user, err := h.useCase.GetUserByID(userID)
	if err != nil {
		return h.handleError(err, "could not get user due internal error")
	}

	return c.JSON(http.StatusOK, user)
}

func (h *Handler) updateUserSettings(c echo.Context) error {
	token := c.Get("user").(*jwt.Token)
	claims := token.Claims.(*entity.TokenClaims)
	userID := claims.ID

	d := new(dto.UpdateSettingsDTO)
	if err := c.Bind(d); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "could not decode data")
	}
	if err := c.Validate(d); err != nil {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, err)
	}

	u, err := h.useCase.UpdateUserSettings(userID, d)
	if err != nil {
		return h.handleError(err, "could not update user settings due internal error")
	}

	return c.JSON(http.StatusOK, u)
}
