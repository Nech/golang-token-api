package v1

import (
	"errors"
	"net/http"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/Nech/golang-token-api/internal/controller/http/dto"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
)

var (
	TestUseCase = new(UseCaseMock)
	TestHandler Handler
)

func init() {
	e := echo.New()
	e.Logger.SetLevel(99) // disable logging in tests

	TestHandler = Handler{
		server:  e,
		useCase: TestUseCase,
	}
}

type UseCaseMock struct {
	mock.Mock
}

func (m *UseCaseMock) GetAllUsers() ([]*entity.UserProfile, error) {
	args := m.Called()
	return args.Get(0).([]*entity.UserProfile), args.Error(1)
}
func (m *UseCaseMock) GetUserByID(id string) (*entity.UserProfile, error) {
	args := m.Called(id)
	return args.Get(0).(*entity.UserProfile), args.Error(1)
}
func (m *UseCaseMock) UpdateUserSettings(userID string, d *dto.UpdateSettingsDTO) (*entity.UserProfile, error) {
	args := m.Called(userID, d)
	return args.Get(0).(*entity.UserProfile), args.Error(1)
}
func (m *UseCaseMock) Login(d *dto.LoginUserDTO) (*entity.TokenPair, error) {
	args := m.Called(d)
	return args.Get(0).(*entity.TokenPair), args.Error(1)
}
func (m *UseCaseMock) Logout(userID string) {
	_ = m.Called(userID)
}
func (m *UseCaseMock) UpdateAccessToken(auth string) (*entity.Token, error) {
	args := m.Called(auth)
	return args.Get(0).(*entity.Token), args.Error(1)
}
func (m *UseCaseMock) ParseToken(auth string, isRefresh bool) (interface{}, error) {
	args := m.Called(auth, isRefresh)
	return args.Get(0).(interface{}), args.Error(1)
}
func (m *UseCaseMock) CheckAccessToken(userID, tokenUID string) bool {
	args := m.Called(userID, tokenUID)
	return args.Get(0).(bool)
}

func TestHandleError(t *testing.T) {
	msg := "Critical error"

	res := TestHandler.handleError(nil, msg)
	assert.Nil(t, res)

	err := apperror.ErrorNotFound
	res = TestHandler.handleError(err, msg)
	assert.Equal(t, echo.NewHTTPError(http.StatusNotFound, err.Message), res)

	err = apperror.ErrorDuplicateKey
	res = TestHandler.handleError(err, msg)
	assert.Equal(t, echo.NewHTTPError(http.StatusBadRequest, err.Message), res)

	err = apperror.ErrorWrongID
	res = TestHandler.handleError(err, msg)
	assert.Equal(t, echo.NewHTTPError(http.StatusBadRequest, err.Message), res)

	err = apperror.ErrorWrongAuthData
	res = TestHandler.handleError(err, msg)
	assert.Equal(t, echo.NewHTTPError(http.StatusUnauthorized, err.Message), res)

	err = apperror.ErrorWrongTokenSign
	res = TestHandler.handleError(err, msg)
	assert.Equal(t, echo.NewHTTPError(http.StatusUnauthorized, err.Message), res)

	err = apperror.ErrorTokenInvalid
	res = TestHandler.handleError(err, msg)
	assert.Equal(t, echo.NewHTTPError(http.StatusUnauthorized, err.Message), res)

	err = apperror.ErrorTokenNotFound
	res = TestHandler.handleError(err, msg)
	assert.Equal(t, echo.NewHTTPError(http.StatusUnauthorized, err.Message), res)

	er := errors.New("internal error")
	res = TestHandler.handleError(er, msg)
	assert.Equal(t, echo.NewHTTPError(http.StatusInternalServerError, msg), res)
}
