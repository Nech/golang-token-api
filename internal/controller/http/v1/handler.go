package v1

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/Nech/golang-token-api/internal/controller/http/dto"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
)

// Handler structure for working with the server
type Handler struct {
	server  *echo.Echo
	useCase UseCaseI
}

func NewHandler(server *echo.Echo, useCase UseCaseI) *Handler {
	return &Handler{server: server, useCase: useCase}
}

// UseCaseI describes the common interface
type UseCaseI interface {
	GetAllUsers() ([]*entity.UserProfile, error)
	GetUserByID(id string) (*entity.UserProfile, error)
	UpdateUserSettings(userID string, d *dto.UpdateSettingsDTO) (*entity.UserProfile, error)
	Login(d *dto.LoginUserDTO) (*entity.TokenPair, error)
	Logout(userID string)
	UpdateAccessToken(refreshToken string) (*entity.Token, error)
	CheckAccessToken(userID, tokenUID string) bool
	ParseToken(auth string, isRefresh bool) (interface{}, error)
}

// InitRoutes registers the API routing
func (h *Handler) InitRoutes() {
	// API V1
	v1 := h.server.Group("/v1")

	// Auth routes
	authRoutes := v1.Group("/auth")
	authRoutes.POST("/sign-in", h.signIn)
	authRoutes.POST("/refresh", h.refreshToken)
	authRoutes.POST("/sign-out", h.signOut, h.jwtMiddleware(), h.cachedTokenMiddleware())

	// User routes
	userRoutes := v1.Group("/user", h.jwtMiddleware(), h.cachedTokenMiddleware())
	userRoutes.GET("", h.getUsers)
	userRoutes.GET("/:id", h.getUserByID)
	userRoutes.GET("/me", h.getUserByToken)
	userRoutes.PATCH("/settings", h.updateUserSettings)

}

// handleError helps to handle the received error
func (h *Handler) handleError(err error, criticalErrorMessage string) error {
	if err == nil {
		return nil
	}

	ae, ok := err.(*apperror.AppError)
	if ok && ae.HttpCode != 500 {
		// common http error
		return echo.NewHTTPError(ae.HttpCode, ae.Message)
	}

	// critical error
	h.server.Logger.Errorf("%s: %v", criticalErrorMessage, err)
	return echo.NewHTTPError(http.StatusInternalServerError, criticalErrorMessage)
}
