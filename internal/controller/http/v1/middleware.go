package v1

import (
	"net/http"

	"github.com/golang-jwt/jwt"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
)

// jwtMiddleware validates the user's authorization token
func (h *Handler) jwtMiddleware() echo.MiddlewareFunc {
	config := echojwt.Config{
		ParseTokenFunc: func(c echo.Context, auth string) (interface{}, error) {
			return h.useCase.ParseToken(auth, false)
		},
	}

	return echojwt.WithConfig(config)
}

// cachedTokenMiddleware checks the token ID in the cache
func (h *Handler) cachedTokenMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			token := c.Get("user").(*jwt.Token)
			claims := token.Claims.(*entity.TokenClaims)

			if !h.useCase.CheckAccessToken(claims.ID, claims.UID) {
				return c.JSON(http.StatusUnauthorized, echo.Map{"message": "jwt is invalid"})
			}

			return next(c)
		}
	}
}
