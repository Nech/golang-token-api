package config

import (
	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
)

// Config is a structure for accessing secret data of application
type Config struct {
	ApiPort                int    `env:"API_PORT"`
	AccessTokenKey         string `env:"ACCESS_TOKEN_SIGNING_KEY"`
	RefreshTokenKey        string `env:"REFRESH_TOKEN_SIGNING_KEY"`
	AccessTokenTime        int    `env:"ACCESS_TOKEN_TIME"`
	RefreshTokenTime       int    `env:"REFRESH_TOKEN_TIME"`
	RedisHost              string `env:"REDIS_HOST"`
	RedisPort              int    `env:"REDIS_PORT"`
	RedisDb                int    `env:"REDIS_DB"`
	MongoDbName            string `env:"MONGO_DB_NAME"`
	MongoDbHost            string `env:"MONGO_DB_HOST"`
	MongoDbPort            int    `env:"MONGO_DB_PORT"`
	MongoDbUsersCollection string `env:"MONGO_DB_USERS_COLLECTION"`
}

// Setup returns a link to the configuration structure
func Setup() (*Config, error) {
	var cfg = &Config{}
	err := loadEnvVariables()
	if err != nil {
		return nil, apperror.NewFatalError(err, "can not load environment variables")
	}

	err = parseEnvVariables(cfg)
	if err != nil {
		return nil, apperror.NewFatalError(err, "can not parse environment variables")
	}

	return cfg, nil
}

// loadEnvVariables load data from environment (read .env file)
func loadEnvVariables() error {
	return godotenv.Load()
}

// parseEnvVariables parse environment variables into `Config`
func parseEnvVariables(cfg *Config) error {
	opts := env.Options{RequiredIfNoDef: true} // all structure fields are required
	return env.Parse(cfg, opts)
}
