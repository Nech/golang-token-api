package config

import (
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseEnvVariables(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		const (
			apiPort                = 6789
			accessTokenKey         = "foo"
			refreshTokenKey        = "bar"
			accessTokenTime        = 10
			refreshTokenTime       = 20
			redisHost              = "redis"
			redisPort              = 6677
			redisDb                = 10
			mongoDbName            = "db-test"
			mongoDbHost            = "localhost"
			mongoDbPort            = 12345
			mongoDbUsersCollection = "test1"
		)

		os.Setenv("API_PORT", strconv.Itoa(apiPort))
		os.Setenv("ACCESS_TOKEN_SIGNING_KEY", accessTokenKey)
		os.Setenv("REFRESH_TOKEN_SIGNING_KEY", refreshTokenKey)
		os.Setenv("ACCESS_TOKEN_TIME", strconv.Itoa(accessTokenTime))
		os.Setenv("REFRESH_TOKEN_TIME", strconv.Itoa(refreshTokenTime))
		os.Setenv("REDIS_HOST", redisHost)
		os.Setenv("REDIS_PORT", strconv.Itoa(redisPort))
		os.Setenv("REDIS_DB", strconv.Itoa(redisDb))
		os.Setenv("MONGO_DB_NAME", mongoDbName)
		os.Setenv("MONGO_DB_HOST", mongoDbHost)
		os.Setenv("MONGO_DB_PORT", strconv.Itoa(mongoDbPort))
		os.Setenv("MONGO_DB_USERS_COLLECTION", mongoDbUsersCollection)

		var cfg = &Config{}
		_ = parseEnvVariables(cfg)

		assert.Equal(t, apiPort, cfg.ApiPort)
		assert.Equal(t, accessTokenKey, cfg.AccessTokenKey)
		assert.Equal(t, refreshTokenKey, cfg.RefreshTokenKey)
		assert.Equal(t, accessTokenTime, cfg.AccessTokenTime)
		assert.Equal(t, refreshTokenTime, cfg.RefreshTokenTime)
		assert.Equal(t, redisHost, cfg.RedisHost)
		assert.Equal(t, redisPort, cfg.RedisPort)
		assert.Equal(t, redisDb, cfg.RedisDb)
		assert.Equal(t, mongoDbName, cfg.MongoDbName)
		assert.Equal(t, mongoDbHost, cfg.MongoDbHost)
		assert.Equal(t, mongoDbPort, cfg.MongoDbPort)
		assert.Equal(t, mongoDbUsersCollection, cfg.MongoDbUsersCollection)
	})

	t.Run("error", func(t *testing.T) {
		os.Clearenv()
		var cfg = &Config{}
		err := parseEnvVariables(cfg)

		assert.Error(t, err)
		assert.Contains(t, err.Error(), "env: required environment variable")
	})
}
