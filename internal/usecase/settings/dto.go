package settings

type UpdateUserSettingsDTO struct {
	ID          string
	Lang        string
	Ripple      bool
	InputStyle  string
	MenuMode    string
	ColorScheme string
	Theme       string
	Scale       int32
	UpdatedAt   int64
}
