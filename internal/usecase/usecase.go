package usecase

import (
	"log"
	"strings"

	"gitlab.com/Nech/golang-token-api/internal/controller/http/dto"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/internal/usecase/settings"
	"gitlab.com/Nech/golang-token-api/internal/usecase/user"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
)

type UseCase struct {
	authService       AuthServiceI
	userService       UserServiceI
	tokenCacheService TokenCacheServiceI
}

func NewUseCase(authService AuthServiceI, userService UserServiceI, tokenService TokenCacheServiceI) *UseCase {
	return &UseCase{authService: authService, userService: userService, tokenCacheService: tokenService}
}

// AuthServiceI describe AuthService interface
type AuthServiceI interface {
	GenerateHash(str string) string
	GenerateTokens(userID string) (tokens *entity.TokenPair, cached *entity.CachedTokens)
	CheckRefreshToken(auth string) (userID, tokenUID string, err error)
	ParseToken(auth string, isRefresh bool) (interface{}, error)
	UpdateCachedTokens(userID, ruid string) (cached *entity.CachedTokens, auth string)
}

// TokenCacheServiceI describe TokenService interface
type TokenCacheServiceI interface {
	ValidateCachedToken(userID, tokenUID string, isRefresh bool) bool
	SaveCachedTokens(cached *entity.CachedTokens, userID string)
	DeleteCachedTokens(userID string)
}

// UserServiceI describe UserService interface
type UserServiceI interface {
	GetAll() ([]*entity.UserProfile, error)
	GetByID(id string) (*entity.UserProfile, error)
	GetAuthData(nickname string) (*entity.UserAuthData, error)
	UpdateUserSettings(d *settings.UpdateUserSettingsDTO) (*entity.UserProfile, error)
	GetCount() (int64, error)
	CreateIndexes() error
	AppendUser(d *user.CreateUserProfileDTO) (*entity.UserID, error)
}

// GetAllUsers returns list of all users
func (u *UseCase) GetAllUsers() ([]*entity.UserProfile, error) {
	return u.userService.GetAll()
}

// GetUserByID returns user by passed ID
func (u *UseCase) GetUserByID(id string) (*entity.UserProfile, error) {
	return u.userService.GetByID(id)
}

// Login checks the user's nickname and password and returns authorization tokens
func (u *UseCase) Login(d *dto.LoginUserDTO) (*entity.TokenPair, error) {
	passwordHash := u.authService.GenerateHash(d.Password)
	authData, err := u.userService.GetAuthData(d.Nickname)

	// check credentials
	if err != nil {
		return nil, err
	}
	if passwordHash != authData.PasswordHash {
		return nil, apperror.ErrorWrongAuthData
	}

	// ok, create new tokens
	tokens, cached := u.authService.GenerateTokens(authData.ID)

	// save tokens UID to cache
	u.tokenCacheService.SaveCachedTokens(cached, authData.ID)

	return tokens, nil
}

// Logout deletes cached tokens of the user
func (u *UseCase) Logout(userID string) {
	u.tokenCacheService.DeleteCachedTokens(userID)
}

// UpdateAccessToken Validates the refresh token and generates a new access token
func (u *UseCase) UpdateAccessToken(auth string) (*entity.Token, error) {
	// check refresh token, get payload
	userID, ruid, err := u.authService.CheckRefreshToken(auth)
	if err != nil {
		return nil, err
	}

	// check in the cache
	isValid := u.tokenCacheService.ValidateCachedToken(userID, ruid, true)
	if !isValid {
		return nil, apperror.ErrorTokenInvalid
	}

	// get new CachedToken and access token
	cached, at := u.authService.UpdateCachedTokens(userID, ruid)

	// save tokens UID to cache
	u.tokenCacheService.SaveCachedTokens(cached, userID)

	// return access token
	return &entity.Token{AccessToken: at}, nil
}

// ParseToken used to validate the passed JWT token
func (u *UseCase) ParseToken(auth string, isRefresh bool) (interface{}, error) {
	return u.authService.ParseToken(auth, isRefresh)
}

// CheckAccessToken checks token IDs in the cache
func (u *UseCase) CheckAccessToken(userID, tokenUID string) bool {
	return u.tokenCacheService.ValidateCachedToken(userID, tokenUID, false)
}

// UpdateUserSettings changes user settings
func (u *UseCase) UpdateUserSettings(userID string, d *dto.UpdateSettingsDTO) (*entity.UserProfile, error) {
	pref := &settings.UpdateUserSettingsDTO{
		ID:          userID,
		Lang:        strings.ToLower(strings.TrimSpace(d.Lang)),
		Ripple:      d.Ripple,
		InputStyle:  strings.ToLower(strings.TrimSpace(d.InputStyle)),
		MenuMode:    strings.ToLower(strings.TrimSpace(d.MenuMode)),
		ColorScheme: strings.ToLower(strings.TrimSpace(d.ColorScheme)),
		Theme:       strings.ToLower(strings.TrimSpace(d.Theme)),
		Scale:       d.Scale,
	}

	return u.userService.UpdateUserSettings(pref)
}

// CheckUsers Launches at application startup, creates indexes and test users
func (u *UseCase) CheckUsers() error {
	// get count users
	count, err := u.userService.GetCount()
	if err != nil {
		return err
	}
	if count > 0 {
		return nil
	}

	// create indexes in the db
	err = u.userService.CreateIndexes()
	if err != nil {
		return err
	}
	log.Println("Indexes created")

	// append users
	users := user.GetUsers()
	for _, d := range users {
		id, err := u.userService.AppendUser(d)
		if err != nil {
			return err
		}
		log.Printf("Added d %s with ID %s\n", d.Nickname, id)
	}

	return nil
}
