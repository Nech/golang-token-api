package user

import "time"

var passwordHash = "6466736173686775797277653433326666775baa61e4c9b93f3f0682250b6cf8331b7ee68fd8"

func GetUsers() []*CreateUserProfileDTO {
	var users []*CreateUserProfileDTO

	users = append(users, &CreateUserProfileDTO{
		Nickname:     "super-admin",
		Roles:        getSuperAdminRoles(),
		PasswordHash: passwordHash,
		CreatedAt:    time.Now().Unix(),
		UpdatedAt:    0,
	})

	users = append(users, &CreateUserProfileDTO{
		Nickname:     "admin",
		Roles:        getAdminRoles(),
		PasswordHash: passwordHash,
		CreatedAt:    time.Now().Unix(),
		UpdatedAt:    0,
	})

	users = append(users, &CreateUserProfileDTO{
		Nickname:     "manager",
		Roles:        getManagerRoles(),
		PasswordHash: passwordHash,
		CreatedAt:    time.Now().Unix(),
		UpdatedAt:    0,
	})

	users = append(users, &CreateUserProfileDTO{
		Nickname:     "user",
		Roles:        getUserRoles(),
		PasswordHash: passwordHash,
		CreatedAt:    time.Now().Unix(),
		UpdatedAt:    0,
	})

	return users
}

func getSuperAdminRoles() []string {
	var roles []string
	roles = append(roles, "ROLE_SUPER_ADMIN")
	roles = append(roles, "ROLE_USER")

	return roles
}

func getAdminRoles() []string {
	var roles []string
	roles = append(roles, "ROLE_ADMIN")
	roles = append(roles, "ROLE_USER")

	return roles
}

func getManagerRoles() []string {
	var roles []string
	roles = append(roles, "ROLE_MANAGER")
	roles = append(roles, "ROLE_USER")

	return roles
}

func getUserRoles() []string {
	var roles []string
	roles = append(roles, "ROLE_USER")

	return roles
}
