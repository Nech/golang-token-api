package user

import (
	"gitlab.com/Nech/golang-token-api/internal/adapter/db/mongodb/mongodb_model"
)

type CreateUserProfileDTO struct {
	Nickname     string
	Roles        []string
	PasswordHash string
	CreatedAt    int64
	UpdatedAt    int64
}

type UpdateUserRoleDTO struct {
	ID        string
	Roles     []string
	UpdatedAt int64
}

func (d *CreateUserProfileDTO) ToMongodbUser() *mongodb_model.User {
	return &mongodb_model.User{
		Nickname:     d.Nickname,
		Roles:        d.Roles,
		PasswordHash: d.PasswordHash,
		Settings: &mongodb_model.Settings{
			Lang:        "ukr",
			Ripple:      false,
			InputStyle:  "outlined",
			MenuMode:    "static",
			ColorScheme: "light",
			Theme:       "lara-light-blue",
			Scale:       14,
		},
		CreatedAt: d.CreatedAt,
		UpdatedAt: 0,
	}
}
