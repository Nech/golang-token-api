package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/Nech/golang-token-api/internal/controller/http/dto"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/internal/usecase/settings"
	"gitlab.com/Nech/golang-token-api/internal/usecase/user"
)

const (
	TestID             = "a050fdc4-fe76-446a-bee4-dbd1cb0510b5"
	TestNickname       = "nickname"
	TestPassword       = "password"
	TestPasswordHash   = "foobar"
	TestTimestamp      = 1667919935
	TestAccessToken    = "access-token"
	TestAccessTokenID  = "access-token-id"
	TestRefreshToken   = "refresh-token"
	TestRefreshTokenID = "refresh-token-id"
)

var (
	TestAuthService       = new(AuthServiceMock)
	TestTokenCacheService = new(TokenCacheServiceMock)
	TestUserService       = new(UserServiceMock)
	TestUseCase           = UseCase{
		authService:       TestAuthService,
		tokenCacheService: TestTokenCacheService,
		userService:       TestUserService,
	}

	TestRoles = getTestRoles()

	expectedProfile = &entity.UserProfile{
		ID:        TestID,
		Nickname:  TestNickname,
		Roles:     TestRoles,
		CreatedAt: TestTimestamp,
		UpdatedAt: 0,
	}
)

func getTestRoles() []string {
	var roles []string
	roles = append(roles, "ROLE_USER")

	return roles
}

type AuthServiceMock struct {
	mock.Mock
}
type TokenCacheServiceMock struct {
	mock.Mock
}
type UserServiceMock struct {
	mock.Mock
}

func (m *AuthServiceMock) GenerateHash(str string) string {
	args := m.Called(str)
	return args.Get(0).(string)
}
func (m *AuthServiceMock) GenerateTokens(userID string) (tokens *entity.TokenPair, cached *entity.CachedTokens) {
	args := m.Called(userID)
	return args.Get(0).(*entity.TokenPair), args.Get(1).(*entity.CachedTokens)
}
func (m *AuthServiceMock) CheckRefreshToken(auth string) (userID, tokenUID string, err error) {
	args := m.Called(auth)
	return args.Get(0).(string), args.Get(1).(string), args.Error(2)
}
func (m *AuthServiceMock) ParseToken(auth string, isRefresh bool) (interface{}, error) {
	args := m.Called(auth, isRefresh)
	return args.Get(0).(interface{}), args.Error(1)
}
func (m *AuthServiceMock) UpdateCachedTokens(userID, uid string) (cached *entity.CachedTokens, auth string) {
	args := m.Called(userID, uid)
	return args.Get(0).(*entity.CachedTokens), args.Get(1).(string)
}

func (m *TokenCacheServiceMock) ValidateCachedToken(userID, tokenUID string, isRefresh bool) bool {
	args := m.Called(userID, tokenUID, isRefresh)
	return args.Get(0).(bool)
}
func (m *TokenCacheServiceMock) SaveCachedTokens(cached *entity.CachedTokens, userID string) {
	_ = m.Called(cached, userID)
}
func (m *TokenCacheServiceMock) DeleteCachedTokens(userID string) {
	_ = m.Called(userID)
}

func (m *UserServiceMock) GetAll() ([]*entity.UserProfile, error) {
	args := m.Called()
	return args.Get(0).([]*entity.UserProfile), args.Error(1)
}
func (m *UserServiceMock) GetByID(id string) (*entity.UserProfile, error) {
	args := m.Called(id)
	return args.Get(0).(*entity.UserProfile), args.Error(1)
}
func (m *UserServiceMock) GetAuthData(nickname string) (*entity.UserAuthData, error) {
	args := m.Called(nickname)
	return args.Get(0).(*entity.UserAuthData), args.Error(1)
}
func (m *UserServiceMock) UpdateUserSettings(d *settings.UpdateUserSettingsDTO) (*entity.UserProfile, error) {
	args := m.Called(d)
	return args.Get(0).(*entity.UserProfile), args.Error(1)
}
func (m *UserServiceMock) GetCount() (int64, error) {
	args := m.Called()
	return args.Get(0).(int64), args.Error(1)
}
func (m *UserServiceMock) CreateIndexes() error {
	args := m.Called()
	return args.Error(0)
}
func (m *UserServiceMock) AppendUser(d *user.CreateUserProfileDTO) (*entity.UserID, error) {
	args := m.Called(d)
	return args.Get(0).(*entity.UserID), args.Error(1)
}

func TestUseCase_GetAllUsers(t *testing.T) {
	TestUserService.On("GetAll").Return([]*entity.UserProfile{expectedProfile}, nil)
	res, err := TestUseCase.GetAllUsers()

	assert.Equal(t, expectedProfile, res[0])
	assert.Nil(t, err)
}

func TestUseCase_GetUserByID(t *testing.T) {
	TestUserService.On("GetByID", TestID).Return(expectedProfile, nil)
	res, err := TestUseCase.GetUserByID(TestID)

	assert.Equal(t, expectedProfile, res)
	assert.Nil(t, err)
}

func TestUseCase_Login(t *testing.T) {
	d := &dto.LoginUserDTO{
		Nickname: TestNickname,
		Password: TestPassword,
	}
	expectedAuthData := &entity.UserAuthData{
		ID:           TestID,
		Nickname:     TestNickname,
		PasswordHash: TestPasswordHash,
		Roles:        TestRoles,
	}
	expectedTokenPair := &entity.TokenPair{
		AccessToken:  TestAccessToken,
		RefreshToken: TestRefreshToken,
	}
	expectedCachedTokens := &entity.CachedTokens{
		AccessUID:  TestAccessTokenID,
		RefreshUID: TestRefreshTokenID,
	}
	TestAuthService.On("GenerateHash", TestPassword).Return(TestPasswordHash)
	TestUserService.On("GetAuthData", TestNickname).Return(expectedAuthData, nil)
	TestAuthService.On("GenerateTokens", TestID).Return(expectedTokenPair, expectedCachedTokens)
	TestTokenCacheService.On("SaveCachedTokens", expectedCachedTokens, TestID).Return()
	res, err := TestUseCase.Login(d)

	assert.Equal(t, expectedTokenPair, res)
	assert.Nil(t, err)
	TestTokenCacheService.AssertCalled(t, "SaveCachedTokens", expectedCachedTokens, TestID)
}

func TestUseCase_Logout(t *testing.T) {
	TestTokenCacheService.On("DeleteCachedTokens", TestID).Return()
	TestUseCase.Logout(TestID)

	TestTokenCacheService.AssertCalled(t, "DeleteCachedTokens", TestID)
}

func TestUseCase_UpdateAccessToken(t *testing.T) {
	auth := "foo"
	expectedCachedTokens := &entity.CachedTokens{
		AccessUID:  TestAccessTokenID,
		RefreshUID: TestRefreshTokenID,
	}
	expectedNewToken := &entity.Token{AccessToken: "bar"}

	TestAuthService.On("CheckRefreshToken", auth).Return(TestID, TestRefreshTokenID, nil)
	TestTokenCacheService.On("ValidateCachedToken", TestID, TestRefreshTokenID, true).Return(true)
	TestAuthService.On("UpdateCachedTokens", TestID, TestRefreshTokenID).Return(expectedCachedTokens, "bar")
	TestTokenCacheService.On("SaveCachedTokens", expectedCachedTokens, TestID).Return()
	res, err := TestUseCase.UpdateAccessToken(auth)

	assert.Equal(t, expectedNewToken, res)
	assert.Nil(t, err)
}

func TestUseCase_ParseToken(t *testing.T) {
	auth := "foo"
	expected := "bar"
	TestAuthService.On("ParseToken", auth, true).Return(expected, nil)
	res, err := TestUseCase.ParseToken(auth, true)

	assert.Equal(t, expected, res)
	assert.Nil(t, err)
}

func TestUseCase_CheckAccessToken(t *testing.T) {
	TestTokenCacheService.On("ValidateCachedToken", TestID, TestID, false).Return(true)
	res := TestUseCase.CheckAccessToken(TestID, TestID)

	assert.Equal(t, true, res)
}
