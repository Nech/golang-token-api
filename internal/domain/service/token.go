package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/labstack/gommon/log"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
	"gitlab.com/Nech/golang-token-api/pkg/cacheerror"
)

// TokenCacheService designed to work with cached tokens
type TokenCacheService struct {
	storage TokenStorageI
}

// NewTokenCacheService is the service constructor
func NewTokenCacheService(storage TokenStorageI) *TokenCacheService {
	return &TokenCacheService{storage: storage}
}

// TokenStorageI storage interface in Redis
type TokenStorageI interface {
	Set(ctx context.Context, key string, value []byte, exp time.Duration) error
	Get(ctx context.Context, key string) ([]byte, error)
	Delete(ctx context.Context, key string)
}

// ValidateCachedToken checks the ID of the saved token
func (s *TokenCacheService) ValidateCachedToken(userID, tokenUID string, isRefresh bool) bool {
	cachedTokens, err := s.getCachedTokens(userID)
	if err != nil && errors.Is(err, &cacheerror.CacheError{}) {
		log.Error(err)
		// ignore the error if the redis service is not available!
		return true
	} else if err != nil {
		return false
	}

	uid := cachedTokens.AccessUID
	if isRefresh {
		uid = cachedTokens.RefreshUID
	}

	if tokenUID == uid {
		return true
	}

	return false
}

// SaveCachedTokens saves token IDs in the cache
func (s *TokenCacheService) SaveCachedTokens(cached *entity.CachedTokens, userID string) {
	cachedJSON, _ := json.Marshal(cached)
	err := s.storage.Set(ctx, fmt.Sprintf("token-%s", userID), cachedJSON, 0)
	if err != nil {
		log.Error(err)
	}
}

// DeleteCachedTokens delete user tokens in the cache
func (s *TokenCacheService) DeleteCachedTokens(userID string) {
	s.storage.Delete(ctx, fmt.Sprintf("token-%s", userID))
}

// getCachedTokens receives cached tokens by user ID
func (s *TokenCacheService) getCachedTokens(userID string) (*entity.CachedTokens, error) {
	cachedJSON, err := s.storage.Get(ctx, fmt.Sprintf("token-%s", userID))
	if err != nil {
		return nil, err
	}

	cachedTokens := new(entity.CachedTokens)
	err = json.Unmarshal(cachedJSON, cachedTokens)
	if err != nil {
		return nil, apperror.ErrorTokenInvalid
	}

	return cachedTokens, nil
}
