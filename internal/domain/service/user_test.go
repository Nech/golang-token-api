package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/internal/usecase/settings"
	"gitlab.com/Nech/golang-token-api/internal/usecase/user"
)

const (
	TestID           = "a050fdc4-fe76-446a-bee4-dbd1cb0510b5"
	TestNickname     = "nickname"
	TestPasswordHash = "foobar"
	TestTimestamp    = 1667919935
)

var (
	TestUserStorage = new(UserStorageMock)
	TestUserService = &UserService{
		storage: TestUserStorage,
	}

	TestRoles = getTestRoles()

	expectedProfile = &entity.UserProfile{
		ID:        TestID,
		Nickname:  TestNickname,
		Roles:     TestRoles,
		CreatedAt: TestTimestamp,
		UpdatedAt: 0,
	}
)

func getTestRoles() []string {
	var roles []string
	roles = append(roles, "ROLE_USER")

	return roles
}

type UserStorageMock struct {
	mock.Mock
}

func (m *UserStorageMock) GetAll(ctx context.Context) ([]*entity.UserProfile, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*entity.UserProfile), args.Error(1)
}
func (m *UserStorageMock) GetByID(ctx context.Context, id string) (*entity.UserProfile, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*entity.UserProfile), args.Error(1)
}
func (m *UserStorageMock) GetAuthData(ctx context.Context, nickname string) (*entity.UserAuthData, error) {
	args := m.Called(ctx, nickname)
	return args.Get(0).(*entity.UserAuthData), args.Error(1)
}
func (m *UserStorageMock) UpdateUserSettings(ctx context.Context, d *settings.UpdateUserSettingsDTO) (*entity.UserProfile, error) {
	args := m.Called(ctx, d)
	return args.Get(0).(*entity.UserProfile), args.Error(1)
}
func (m *UserStorageMock) GetCount(ctx context.Context) (int64, error) {
	args := m.Called(ctx)
	return args.Get(0).(int64), args.Error(1)
}
func (m *UserStorageMock) CreateIndexes(ctx context.Context) error {
	args := m.Called(ctx)
	return args.Error(0)
}
func (m *UserStorageMock) AppendUser(ctx context.Context, d *user.CreateUserProfileDTO) (*entity.UserID, error) {
	args := m.Called(ctx, d)
	return args.Get(0).(*entity.UserID), args.Error(1)
}

func TestUserService_GetAll(t *testing.T) {
	TestUserStorage.On("GetAll", mock.Anything).Return([]*entity.UserProfile{expectedProfile}, nil)
	res, err := TestUserService.GetAll()

	assert.Equal(t, expectedProfile, res[0])
	assert.Nil(t, err)
}

func TestUserService_GetByID(t *testing.T) {
	TestUserStorage.On("GetByID", mock.Anything, TestID).Return(expectedProfile, nil)
	res, err := TestUserService.GetByID(TestID)

	assert.Equal(t, expectedProfile, res)
	assert.Nil(t, err)
}

func TestUserService_GetAuthData(t *testing.T) {
	expectedData := &entity.UserAuthData{
		ID:           TestID,
		Nickname:     TestNickname,
		PasswordHash: TestPasswordHash,
		Roles:        TestRoles,
	}
	TestUserStorage.On("GetAuthData", mock.Anything, TestNickname).Return(expectedData, nil)
	res, err := TestUserService.GetAuthData(TestNickname)

	assert.Equal(t, expectedData, res)
	assert.Nil(t, err)
}
