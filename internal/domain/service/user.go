package service

import (
	"context"
	"time"

	"gitlab.com/Nech/golang-token-api/internal/usecase/settings"

	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/internal/usecase/user"
)

type UserService struct {
	storage UserStorageI
}

func NewUserService(storage UserStorageI) *UserService {
	return &UserService{storage: storage}
}

type UserStorageI interface {
	GetAll(ctx context.Context) ([]*entity.UserProfile, error)
	GetByID(ctx context.Context, id string) (*entity.UserProfile, error)
	GetAuthData(ctx context.Context, nickname string) (*entity.UserAuthData, error)
	UpdateUserSettings(ctx context.Context, d *settings.UpdateUserSettingsDTO) (*entity.UserProfile, error)
	GetCount(ctx context.Context) (int64, error)
	CreateIndexes(ctx context.Context) error
	AppendUser(ctx context.Context, d *user.CreateUserProfileDTO) (*entity.UserID, error)
}

func (s *UserService) GetAll() ([]*entity.UserProfile, error) {
	cont, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return s.storage.GetAll(cont)
}
func (s *UserService) GetByID(id string) (*entity.UserProfile, error) {
	cont, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return s.storage.GetByID(cont, id)
}
func (s *UserService) GetAuthData(nickname string) (*entity.UserAuthData, error) {
	cont, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return s.storage.GetAuthData(cont, nickname)
}

func (s *UserService) UpdateUserSettings(d *settings.UpdateUserSettingsDTO) (*entity.UserProfile, error) {
	cont, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	d.UpdatedAt = time.Now().Unix()

	return s.storage.UpdateUserSettings(cont, d)
}

func (s *UserService) GetCount() (int64, error) {
	cont, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return s.storage.GetCount(cont)
}

func (s *UserService) CreateIndexes() error {
	cont, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return s.storage.CreateIndexes(cont)
}

func (s *UserService) AppendUser(d *user.CreateUserProfileDTO) (*entity.UserID, error) {
	cont, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return s.storage.AppendUser(cont, d)
}
