package service

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
)

const (
	TestAccessTokenID  = "access-token-id"
	TestRefreshTokenID = "refresh-token-id"
)

var (
	TestTokenStorage      = new(TokenStorageMock)
	TestTokenCacheService = &TokenCacheService{storage: TestTokenStorage}

	expectedCachedToken = &entity.CachedTokens{
		AccessUID:  TestAccessTokenID,
		RefreshUID: TestRefreshTokenID,
	}
)

type TokenStorageMock struct {
	mock.Mock
}

func (m *TokenStorageMock) Set(ctx context.Context, key string, value []byte, exp time.Duration) error {
	args := m.Called(ctx, key, value, exp)
	return args.Error(0)
}
func (m *TokenStorageMock) Get(ctx context.Context, key string) ([]byte, error) {
	args := m.Called(ctx, key)
	return args.Get(0).([]byte), args.Error(1)
}
func (m *TokenStorageMock) Delete(ctx context.Context, key string) {
	_ = m.Called(ctx, key)
	return
}

func TestTokenCacheService_ValidateCachedToken(t *testing.T) {
	cachedJSON, _ := json.Marshal(expectedCachedToken)
	TestTokenStorage.On("Get", mock.Anything, fmt.Sprintf("token-%s", TestID)).Return(cachedJSON, nil)
	res := TestTokenCacheService.ValidateCachedToken(TestID, TestAccessTokenID, false)

	assert.True(t, res)
}

func TestTokenCacheService_SaveCachedTokens(t *testing.T) {
	cachedJSON, _ := json.Marshal(expectedCachedToken)
	TestTokenStorage.On("Set", context.Background(), fmt.Sprintf("token-%s", TestID), cachedJSON, time.Duration(0)).Return(nil)
	TestTokenCacheService.SaveCachedTokens(expectedCachedToken, TestID)

	TestTokenStorage.AssertCalled(t, "Set", context.Background(), fmt.Sprintf("token-%s", TestID), cachedJSON, time.Duration(0))
}

func TestTokenCacheService_DeleteCachedTokens(t *testing.T) {
	TestTokenStorage.On("Delete", context.Background(), fmt.Sprintf("token-%s", TestID)).Return()
	TestTokenCacheService.DeleteCachedTokens(TestID)

	TestTokenStorage.AssertCalled(t, "Delete", context.Background(), fmt.Sprintf("token-%s", TestID))
}
