package service

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	TestAccessTokenKey  = "foo"
	TestRefreshTokenKey = "bar"
	TestAccessTokenTL   = 60 * time.Second
	TestRefreshTokenTL  = 120 * time.Second
)

var TestAuthService = &AuthService{
	accessTokenKey:  TestAccessTokenKey,
	refreshTokenKey: TestRefreshTokenKey,
	accessTokenTL:   TestAccessTokenTL,
	refreshTokenTL:  TestRefreshTokenTL,
}

func TestAuthService_GenerateHash(t *testing.T) {
	str := "baz"
	res := TestAuthService.GenerateHash(str)

	assert.IsType(t, "", res)
	assert.NotEqual(t, res, str)
}
