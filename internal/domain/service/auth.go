package service

import (
	"context"
	"crypto/sha1"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
)

const HashSalt = "dfsashguyrwe432ffw"

var ctx = context.Background()

// AuthService auxiliary service for working with encryption, tokens, etc.
type AuthService struct {
	accessTokenKey  string
	refreshTokenKey string
	accessTokenTL   time.Duration
	refreshTokenTL  time.Duration
}

// NewAuthService create new AuthService
func NewAuthService(
	accessTokenKey string,
	refreshTokenKey string,
	accessTokenTL time.Duration,
	refreshTokenTL time.Duration) *AuthService {
	return &AuthService{
		accessTokenKey:  accessTokenKey,
		refreshTokenKey: refreshTokenKey,
		accessTokenTL:   accessTokenTL,
		refreshTokenTL:  refreshTokenTL,
	}
}

// GenerateHash serves to encrypt the password
func (s *AuthService) GenerateHash(str string) string {
	hash := sha1.New()
	hash.Write([]byte(str))

	return fmt.Sprintf("%x", hash.Sum([]byte(HashSalt)))
}

// GenerateTokens creates new user tokens
func (s *AuthService) GenerateTokens(userID string) (tokens *entity.TokenPair, cached *entity.CachedTokens) {
	// generate new TokenPair
	at, auid := generateToken(userID, s.accessTokenKey, s.accessTokenTL)
	rt, ruid := generateToken(userID, s.refreshTokenKey, s.refreshTokenTL)

	tokens = &entity.TokenPair{
		AccessToken:  at,
		RefreshToken: rt,
	}
	cached = &entity.CachedTokens{
		AccessUID:  auid,
		RefreshUID: ruid,
	}

	return
}

// CheckRefreshToken parse token and return its claims
func (s *AuthService) CheckRefreshToken(auth string) (userID, tokenUID string, err error) {
	// parse refresh token
	t, err := s.ParseToken(auth, true)
	if err != nil {
		err = apperror.ErrorTokenInvalid
		return
	}

	// ok, token is valid - get userID and token UID
	token := t.(*jwt.Token)
	claims := token.Claims.(*entity.TokenClaims)

	userID = claims.ID
	tokenUID = claims.UID

	return
}

// ParseToken check is passed token valid
func (s *AuthService) ParseToken(auth string, isRefresh bool) (interface{}, error) {
	keyFunc := func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, apperror.ErrorWrongTokenSign
		}

		key := s.accessTokenKey
		if isRefresh {
			key = s.refreshTokenKey
		}

		return []byte(key), nil
	}

	token, err := jwt.ParseWithClaims(auth, &entity.TokenClaims{}, keyFunc)
	if err != nil {
		return nil, err
	}
	if _, ok := token.Claims.(*entity.TokenClaims); ok && token.Valid {
		return token, nil
	}

	return nil, apperror.ErrorTokenInvalid
}

// UpdateCachedTokens create a new access token and CachedTokens
func (s *AuthService) UpdateCachedTokens(userID, ruid string) (cached *entity.CachedTokens, auth string) {
	auth, auid := generateToken(userID, s.accessTokenKey, s.accessTokenTL)

	cached = &entity.CachedTokens{
		AccessUID:  auid,
		RefreshUID: ruid,
	}

	return
}

// generateToken generate a new user token (access or refresh)
func generateToken(userID, key string, tl time.Duration) (auth, uid string) {
	uid = uuid.New().String()
	claims := &entity.TokenClaims{
		ID:  userID,
		UID: uid,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(tl).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	auth, _ = token.SignedString([]byte(key))

	return
}
