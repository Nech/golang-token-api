package entity

type UserSettings struct {
	Lang        string `json:"lang"`
	Ripple      bool   `json:"ripple"`
	InputStyle  string `json:"input_style"`
	MenuMode    string `json:"menu_mode"`
	ColorScheme string `json:"color_scheme"`
	Theme       string `json:"theme"`
	Scale       int32  `json:"scale"`
}
