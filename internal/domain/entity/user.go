package entity

// UserProfile describes the user profile structure
type UserProfile struct {
	ID        string        `json:"id"`
	Nickname  string        `json:"nickname"`
	Roles     []string      `json:"roles"`
	Settings  *UserSettings `json:"settings"`
	CreatedAt int64         `json:"created_at"`
	UpdatedAt int64         `json:"updated_at"`
}

// UserID designed to display the user ID
type UserID struct {
	ID string
}

// UserAuthData designed to get the hash of the user's password and data to build a token
type UserAuthData struct {
	ID           string
	Nickname     string
	PasswordHash string
	Roles        []string
}
