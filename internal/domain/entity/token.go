package entity

import "github.com/golang-jwt/jwt"

// TokenPair designed to display the JWT access & refresh tokens for user
type TokenPair struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

// Token returns JWT access token only
type Token struct {
	AccessToken string `json:"access_token"`
}

// TokenClaims used to pass user ID in the authorization token
type TokenClaims struct {
	ID  string `json:"id"`
	UID string `json:"uid"`
	jwt.StandardClaims
}

// CachedTokens is designed to cache token identifiers
type CachedTokens struct {
	AccessUID  string `json:"access"`
	RefreshUID string `json:"refresh"`
}
