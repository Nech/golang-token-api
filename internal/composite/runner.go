package composite

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/redis/go-redis/v9"
	"gitlab.com/Nech/golang-token-api/internal/adapter/db/mongodb/storage"
	"gitlab.com/Nech/golang-token-api/internal/adapter/db/redis/cache"
	"gitlab.com/Nech/golang-token-api/internal/config"
	v1 "gitlab.com/Nech/golang-token-api/internal/controller/http/v1"
	"gitlab.com/Nech/golang-token-api/internal/domain/service"
	"gitlab.com/Nech/golang-token-api/internal/usecase"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
	"gitlab.com/Nech/golang-token-api/pkg/echovalidator"
	"gitlab.com/Nech/golang-token-api/pkg/mongodbclient"
	"gitlab.com/Nech/golang-token-api/pkg/redisclient"
	"go.mongodb.org/mongo-driver/mongo"
)

// Run receives the elements to launch, collects the config and launches the server
func Run(cfg *config.Config) error {
	// Get Redis client
	redisClient := redisclient.NewClient(cfg.RedisHost, cfg.RedisPort, cfg.RedisDb)

	// Connect to MongoDB and create the dbClient
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	dbClient, err := mongodbclient.NewClient(ctx, cfg.MongoDbHost, cfg.MongoDbPort)
	if err != nil {
		return apperror.NewFatalError(err, "can not initialize mongodb client")
	}
	defer dbClient.Disconnect(context.Background())

	// Initialize UseCase
	useCase := getUseCase(cfg, redisClient, dbClient)

	// Check database, create users
	err = useCase.CheckUsers()
	if err != nil {
		return err
	}

	// Initialize Echo instance
	e := echo.New()
	e.Validator = echovalidator.NewValidator(validator.New())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE, echo.OPTIONS},
	}))
	e.Use(middleware.Logger())

	// Initialize handlers and routing
	h := v1.NewHandler(e, useCase)
	h.InitRoutes()

	// Start server
	s := &http.Server{
		Addr:         fmt.Sprintf(":%d", cfg.ApiPort),
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		if err := e.StartServer(s); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shut down the server with a timeout of 10 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ct, cnc := context.WithTimeout(context.Background(), 10*time.Second)
	defer cnc()
	if err := e.Shutdown(ct); err != nil {
		e.Logger.Fatal(err)
	}

	return nil
}

// getUseCase build UseCase
func getUseCase(cfg *config.Config, redisClient *redis.Client, dbClient *mongo.Client) *usecase.UseCase {
	userStorage := storage.NewUserStorage(dbClient.Database(cfg.MongoDbName), cfg.MongoDbUsersCollection)
	tokenStorage := cache.NewTokenStorage(redisClient)

	authService := service.NewAuthService(
		cfg.AccessTokenKey,
		cfg.RefreshTokenKey,
		time.Duration(cfg.AccessTokenTime)*time.Minute,
		time.Duration(cfg.RefreshTokenTime)*time.Minute,
	)
	userService := service.NewUserService(userStorage)
	tokenCacheService := service.NewTokenCacheService(tokenStorage)

	return usecase.NewUseCase(authService, userService, tokenCacheService)
}
