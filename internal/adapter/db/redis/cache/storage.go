package cache

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
	"gitlab.com/Nech/golang-token-api/pkg/cacheerror"
)

// TokenStorage is structure for cashing tokens uuid
type TokenStorage struct {
	rdb *redis.Client
}

// NewTokenStorage repository constructor
func NewTokenStorage(rdb *redis.Client) *TokenStorage {
	return &TokenStorage{rdb: rdb}
}

// Set saves data on the key with the transferred lifetime
func (s *TokenStorage) Set(ctx context.Context, key string, value []byte, exp time.Duration) error {
	err := s.rdb.Set(ctx, key, value, exp).Err()
	if err != nil {
		return cacheerror.NewCacheError(err, "can not save data to cache")
	}

	return nil
}

// Get receives data by key
func (s *TokenStorage) Get(ctx context.Context, key string) ([]byte, error) {
	value, err := s.rdb.Get(ctx, key).Bytes()

	if err == redis.Nil {
		return nil, apperror.ErrorTokenNotFound
	} else if err != nil {
		return nil, cacheerror.NewCacheError(err, "can not get data from cache")
	}

	return value, nil
}

// Delete key in the storage
func (s *TokenStorage) Delete(ctx context.Context, key string) {
	_ = s.rdb.Del(ctx, key)
}
