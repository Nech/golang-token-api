package cache

import (
	"context"
	"testing"
	"time"

	"gitlab.com/Nech/golang-token-api/pkg/apperror"

	"github.com/stretchr/testify/assert"

	"github.com/alicebob/miniredis"
	"github.com/redis/go-redis/v9"
)

var (
	redisServer *miniredis.Miniredis
	redisClient *redis.Client
	exp         = 0 * time.Second
	ctx         = context.Background()
)

func mockRedis() *miniredis.Miniredis {
	s, err := miniredis.Run()

	if err != nil {
		panic(err)
	}

	return s
}

func setup() {
	redisServer = mockRedis()
	redisClient = redis.NewClient(&redis.Options{
		Addr: redisServer.Addr(),
	})
}

func teardown() {
	redisServer.Close()
}

func TestTokenStorage_Set(t *testing.T) {
	setup()
	defer teardown()
	TestStorage := &TokenStorage{
		rdb: redisClient,
	}

	err := TestStorage.Set(ctx, "foo", []byte("bar"), exp)
	assert.Nil(t, err)
}

func TestTokenStorage_Get(t *testing.T) {
	setup()
	defer teardown()
	TestStorage := &TokenStorage{
		rdb: redisClient,
	}
	redisClient.Set(ctx, "foo", "bar", exp)

	res, err := TestStorage.Get(ctx, "foo")
	assert.Equal(t, []byte("bar"), res)
	assert.Nil(t, err)
}

func TestTokenStorage_Delete(t *testing.T) {
	setup()
	defer teardown()
	TestStorage := &TokenStorage{
		rdb: redisClient,
	}
	redisClient.Set(ctx, "foo", "bar", exp)

	TestStorage.Delete(ctx, "foo")
	_, err := TestStorage.Get(ctx, "foo")

	assert.Equal(t, apperror.ErrorTokenNotFound, err)
}
