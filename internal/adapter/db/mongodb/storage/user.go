package storage

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/Nech/golang-token-api/internal/adapter/db/mongodb/mongodb_model"
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"gitlab.com/Nech/golang-token-api/internal/usecase/settings"
	"gitlab.com/Nech/golang-token-api/internal/usecase/user"
	"gitlab.com/Nech/golang-token-api/pkg/apperror"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type UserStorage struct {
	collection *mongo.Collection
}

func NewUserStorage(db *mongo.Database, collection string) *UserStorage {
	return &UserStorage{collection: db.Collection(collection)}
}

func (s *UserStorage) GetAll(ctx context.Context) ([]*entity.UserProfile, error) {
	filter := bson.M{}
	findOptions := options.Find()
	var result []*entity.UserProfile

	cursor, err := s.collection.Find(ctx, filter, findOptions)
	if err != nil {
		return nil, fmt.Errorf("failed to get users data: %w", err)
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var u mongodb_model.User
		err := cursor.Decode(&u)
		if err != nil {
			return nil, fmt.Errorf("failed to decode u data: %w", err)
		}
		result = append(result, u.ToUserProfile())
	}

	if err = cursor.Err(); err != nil {
		return nil, fmt.Errorf("cursor error: %w", err)
	}

	return result, nil
}

func (s *UserStorage) GetByID(ctx context.Context, userID string) (*entity.UserProfile, error) {
	id, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return nil, apperror.ErrorWrongID
	}

	filter := bson.M{"_id": id}
	var u mongodb_model.User
	err = s.collection.FindOne(ctx, filter).Decode(&u)
	if err == mongo.ErrNoDocuments {
		return nil, apperror.ErrorNotFound
	}
	if err != nil {
		return nil, fmt.Errorf("failed to decode user with id %s: %w", userID, err)
	}

	return u.ToUserProfile(), nil
}

func (s *UserStorage) GetAuthData(ctx context.Context, nickname string) (*entity.UserAuthData, error) {
	filter := bson.M{"nickname": nickname}
	var u mongodb_model.User
	err := s.collection.FindOne(ctx, filter).Decode(&u)

	if err == mongo.ErrNoDocuments {
		return nil, apperror.ErrorNotFound
	}
	if err != nil {
		return nil, fmt.Errorf("failed to decode user with nickname %s: %w", nickname, err)
	}

	return u.ToUserAuthData(), nil
}

func (s *UserStorage) UpdateUserSettings(ctx context.Context, d *settings.UpdateUserSettingsDTO) (*entity.UserProfile, error) {
	id, err := primitive.ObjectIDFromHex(d.ID)
	if err != nil {
		return nil, apperror.ErrorWrongID
	}

	filter := bson.D{{"_id", id}}
	update := bson.D{
		{"$set", bson.D{
			{"settings.lang", d.Lang},
			{"settings.ripple", d.Ripple},
			{"settings.input_style", d.InputStyle},
			{"settings.menu_mode", d.MenuMode},
			{"settings.color_scheme", d.ColorScheme},
			{"settings.theme", d.Theme},
			{"settings.scale", d.Scale},
			{"updated_at", d.UpdatedAt},
		}}}
	opt := options.FindOneAndUpdate().SetReturnDocument(1)

	var u mongodb_model.User
	err = s.collection.FindOneAndUpdate(ctx, filter, update, opt).Decode(&u)
	if err != nil && strings.Contains(err.Error(), "no documents in result") {
		return nil, apperror.ErrorNotFound
	} else if err != nil {
		return nil, fmt.Errorf("failed to execute update user settings query: %w", err)
	}

	return u.ToUserProfile(), nil
}

// GetCount returns the number of users in the collection
func (s *UserStorage) GetCount(ctx context.Context) (int64, error) {
	count, err := s.collection.EstimatedDocumentCount(ctx)
	if err != nil {
		return -1, fmt.Errorf("failed to get records count: %w", err)
	}

	return count, nil
}

// CreateIndexes creates a unique index for the nickname field in the collection
func (s *UserStorage) CreateIndexes(ctx context.Context) error {
	opt := options.Index()
	opt.SetUnique(true)
	index := mongo.IndexModel{Keys: bson.M{"nickname": 1}, Options: opt}

	_, err := s.collection.Indexes().CreateOne(ctx, index)
	if err != nil {
		return fmt.Errorf("can not create index for nickname field: %w", err)
	}

	return nil
}

func (s *UserStorage) AppendUser(ctx context.Context, d *user.CreateUserProfileDTO) (*entity.UserID, error) {
	u := d.ToMongodbUser()
	bytes, err := bson.Marshal(u)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal data: %w", err)
	}
	result, err := s.collection.InsertOne(ctx, bytes)

	if err != nil && strings.Contains(err.Error(), "E11000 duplicate key error") {
		return nil, apperror.ErrorDuplicateKey
	} else if err != nil {
		return nil, fmt.Errorf("failed to create user: %w", err)
	}

	oid, ok := result.InsertedID.(primitive.ObjectID)
	if ok {
		return &entity.UserID{ID: oid.Hex()}, nil
	}

	return nil, fmt.Errorf("failed to convert object id to hex")
}
