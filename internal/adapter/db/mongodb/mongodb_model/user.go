package mongodb_model

import (
	"gitlab.com/Nech/golang-token-api/internal/domain/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID           primitive.ObjectID `bson:"_id,omitempty"`
	Nickname     string             `json:"nickname" bson:"nickname,omitempty"`
	Roles        []string           `json:"roles" bson:"roles,omitempty"`
	PasswordHash string             `json:"password_hash" bson:"password_hash,omitempty"`
	Settings     *Settings          `json:"settings" bson:"settings"`
	CreatedAt    int64              `json:"created_at" bson:"created_at"`
	UpdatedAt    int64              `json:"updated_at" bson:"updated_at"`
}

func (u *User) ToUserProfile() *entity.UserProfile {
	return &entity.UserProfile{
		ID:        u.ID.Hex(),
		Nickname:  u.Nickname,
		Roles:     u.Roles,
		Settings:  u.Settings.toUserSettings(),
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
	}
}

func (u *User) ToUserAuthData() *entity.UserAuthData {
	return &entity.UserAuthData{
		ID:           u.ID.Hex(),
		Nickname:     u.Nickname,
		PasswordHash: u.PasswordHash,
		Roles:        u.Roles,
	}
}

func (u *User) ToUserID() *entity.UserID {
	return &entity.UserID{ID: u.ID.Hex()}
}
