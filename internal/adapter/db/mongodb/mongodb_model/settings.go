package mongodb_model

import "gitlab.com/Nech/golang-token-api/internal/domain/entity"

type Settings struct {
	Lang        string `json:"lang" bson:"lang"`
	Ripple      bool   `json:"ripple" bson:"ripple"`
	InputStyle  string `json:"input_style" bson:"input_style"`
	MenuMode    string `json:"menu_mode" bson:"menu_mode"`
	ColorScheme string `json:"color_scheme" bson:"color_scheme"`
	Theme       string `json:"theme" bson:"theme"`
	Scale       int32  `json:"scale" bson:"scale"`
}

func (s *Settings) toUserSettings() *entity.UserSettings {
	return &entity.UserSettings{
		Lang:        s.Lang,
		Ripple:      s.Ripple,
		InputStyle:  s.InputStyle,
		MenuMode:    s.MenuMode,
		ColorScheme: s.ColorScheme,
		Theme:       s.Theme,
		Scale:       s.Scale,
	}
}
