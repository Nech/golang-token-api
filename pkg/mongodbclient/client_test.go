package mongodbclient

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	TestHost = "foo"
	TestPort = 12345
)

func TestGetClientOptions(t *testing.T) {
	opt := getClientOptions(TestHost, TestPort)

	assert.IsType(t, &options.ClientOptions{}, opt)
	assert.Equal(t, fmt.Sprintf("%s:%d", TestHost, TestPort), opt.Hosts[0])
}
