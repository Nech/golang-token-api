package mongodbclient

import (
	"context"
	"fmt"
	"strconv"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func NewClient(ctx context.Context, host string, port int) (*mongo.Client, error) {
	client, err := mongo.Connect(ctx, getClientOptions(host, port))
	if err != nil {
		return nil, fmt.Errorf("error connecting to database: %w", err)
	}

	if err = client.Ping(ctx, readpref.Primary()); err != nil {
		return nil, fmt.Errorf("can not to ping database: %w", err)
	}

	return client, nil
}

func getClientOptions(host string, port int) *options.ClientOptions {
	prt := strconv.Itoa(port)

	return options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s", host, prt))
}
