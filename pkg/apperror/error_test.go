package apperror

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	testError        = errors.New("foo")
	testErrorMessage = "bar"
)

func TestNewFatalError(t *testing.T) {
	err := NewFatalError(testError, testErrorMessage)

	assert.IsType(t, &AppError{}, err)
	assert.Equal(t, testError, err.Err)
	assert.Equal(t, testErrorMessage, err.Message)
	assert.Equal(t, 500, err.HttpCode)
}

func TestAppError_Error(t *testing.T) {
	err := NewFatalError(testError, testErrorMessage)

	assert.Equal(t, testErrorMessage, err.Error())
}
