package apperror

// AppError structure for storing HTTP errors
type AppError struct {
	Err      error
	Message  string
	HttpCode int
}

var (
	ErrorNotFound = &AppError{
		Err:      nil,
		Message:  "document not found",
		HttpCode: 404,
	}

	ErrorDuplicateKey = &AppError{
		Err:      nil,
		Message:  "passed parameter is not unique",
		HttpCode: 400,
	}

	ErrorWrongID = &AppError{
		Err:      nil,
		Message:  "invalid ID passed",
		HttpCode: 400,
	}

	ErrorWrongAuthData = &AppError{
		Err:      nil,
		Message:  "invalid nickname or password passed",
		HttpCode: 401,
	}

	ErrorWrongTokenSign = &AppError{
		Err:      nil,
		Message:  "unexpected token signing method",
		HttpCode: 401,
	}

	ErrorTokenInvalid = &AppError{
		Err:      nil,
		Message:  "invalid token",
		HttpCode: 401,
	}

	ErrorTokenNotFound = &AppError{
		Err:      nil,
		Message:  "token not found",
		HttpCode: 401,
	}
)

func NewFatalError(err error, msg string) *AppError {
	return &AppError{
		Err:      err,
		Message:  msg,
		HttpCode: 500,
	}
}

func (e *AppError) Error() string {
	return e.Message
}
