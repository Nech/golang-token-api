package cacheerror

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	testError        = errors.New("foo")
	testErrorMessage = "bar"
)

func TestNewCacheError(t *testing.T) {
	err := NewCacheError(testError, testErrorMessage)

	assert.IsType(t, &CacheError{}, err)
	assert.Equal(t, testError, err.Err)
	assert.Equal(t, testErrorMessage, err.Message)
}

func TestCacheError_Error(t *testing.T) {
	err := NewCacheError(testError, testErrorMessage)

	assert.Equal(t, testErrorMessage, err.Error())
}
