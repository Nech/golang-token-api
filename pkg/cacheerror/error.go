package cacheerror

// CacheError structure for storing cached data errors
type CacheError struct {
	Err     error
	Message string
}

func NewCacheError(err error, msg string) *CacheError {
	return &CacheError{
		Err:     err,
		Message: msg,
	}
}

func (e *CacheError) Error() string {
	return e.Message
}
