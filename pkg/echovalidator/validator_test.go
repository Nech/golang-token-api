package echovalidator

import (
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
)

var val = validator.New()

func TestNewValidator(t *testing.T) {
	item := NewValidator(val)

	assert.IsType(t, &Validator{}, item)
	assert.Equal(t, val, item.validator)
}
