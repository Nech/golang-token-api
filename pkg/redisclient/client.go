package redisclient

import (
	"fmt"
	"strconv"

	"github.com/redis/go-redis/v9"
)

func NewClient(host string, port, db int) *redis.Client {
	prt := strconv.Itoa(port)
	addr := fmt.Sprintf("%s:%s", host, prt)

	return redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: "",
		DB:       db,
	})
}
