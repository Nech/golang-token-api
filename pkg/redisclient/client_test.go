package redisclient

import (
	"testing"

	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
)

const (
	testHost = "redis"
	testPort = 6677
	testDb   = 10
)

func TestNewClient(t *testing.T) {
	testClient := NewClient(testHost, testPort, testDb)

	assert.IsType(t, &redis.Client{}, testClient)
}
