# Golang Token API

## Project description

Simple API for working with two authorization tokens.
The token identifiers are saved in redis. 
This way the user will always have only one valid pair of tokens.

## How to start

Use a docker container to start the application.

`docker-compose up --build`

## Environment file

Be sure to create an `.env` file in the root of the project (see the `.env.example` file for an example)